#!/usr/bin/env python
#(c) by Genedata. All rights reserved.
#

from settings import version
__version__ = f'{version}.1'

import os
import math

from pandas.core.frame import DataFrame
import click
import scipy.stats
import numpy as np
import pandas as pd

import statsmodels.api as sm
import pycircos

from natsort import natsort_keygen

Garc= pycircos.Garc
Gcircle= pycircos.Gcircle

import warnings
warnings.simplefilter(action = 'ignore', category = Warning)

VECTOR_SIZE_FACTOR = 10000

GZ_FILE = 'application/gzip'
BZ2_FILE = 'application/x-bzip2'
FASTA_FILE = 'text/plain'
FASTA_FILE_EXT = ['fa', 'fna', 'fasta']

CONTEXT_SETTINGS = {'help_option_names': ['-h', '--help', '--rhonda'],}
@click.version_option(version = __version__)
@click.command(context_settings = CONTEXT_SETTINGS)
@click.option('--vectors',
               type = str,
               required = True,
               help = 'The comma delimited list of the vectors.')
@click.option('--coverage',
               type = str,
               required = True,
               help = 'The file with the scaffold and vector coverages.')
@click.option('--output-ext',
               type = str,
               default = '.region.detection.result.txt',
               show_default = True,
               help = 'The extension of the output file.')
@click.option('--plot-ext',
               type = str,
               default = '_circos.png',
               show_default = True,
               help = 'The extension of the plot file.')
@click.option('--trackname',
               type = str,
               required = True,
               help = 'The name of the track. (i.e ${track_name_0_0})')
@click.option('--plot-segments',
               type = int,
               default = 20,
               show_default = True,
               help = 'The amount of scaffold displayed in the circular plot.')
@click.option('--rolling-segments',
               type = int,
               default = 11,
               show_default = True,
               help = 'The rolling segments for the smoothing.')
@click.option('--high-category',
               type = int,
               default = 11,
               show_default = True,
               help = 'Amount of segments within the upper quantile necessary to be in the high category. This value must not exceed the value of --rolling-segments otherwise no segments can be categorized in the high category.')
@click.option('--mid-category',
               type = int,
               default = 9,
               show_default = True,
               help = 'Amount of segments within the upper quantile necessary to be in the mid category. This value must not exceed the value of --rolling-segments.')
@click.option('--quantile',
               type = float,
               default = 0.95,
               show_default = True,
               help = 'The upper quantile that defines the "high" category.')
@click.option('--pvalue-cutoff',
               type = float,
               default = 0.01,
               show_default = True,
               help = 'The p-value for determine scaffold with significant read density.')
@click.option('--shared-folder',
               type = click.Path(exists = True),
               required = True,
               help = 'The folder for the output files.')
def main(vectors, coverage, output_ext, plot_ext, trackname, plot_segments, rolling_segments, high_category, mid_category, quantile, pvalue_cutoff, shared_folder):
    vector_list = vectors.split(',')
    col_names = ['contig', 'start', 'end', 'contig_id', 'coverage', 'coverage_width', 'segment_size', 'segment_cov', 'contig_len']
    df = pd.read_csv(coverage, names = col_names, header = 0, sep = '\t')

    # calculate the quantiles without the vectors
    df_contigs = df[~df['contig'].isin(vector_list)]

    # uncomment if using >= numpy-1.23
    #top_percentile = np.quantile(df_scaffolds['coverage'], quantile, method = 'linear') 
    top_percentile = np.quantile(df_contigs['coverage'], quantile)
    
    # identify the segments in the top 5% of coverage
    df['perc1'] = df['coverage'] > top_percentile
    df['perc1'].replace({False: 0, True: 1}, inplace = True)

    # create a dataframe with the counts of each category per contig
    df_category_counts = pd.DataFrame(index = ['zero', 'low', 'mid', 'high'], dtype = int)
    for (contig, frame) in df.groupby('contig'):
        frame['sigSum'] = frame['perc1'].rolling(rolling_segments, center = True).sum()
        cond = [frame['sigSum'] == 0, np.logical_and(frame['sigSum'] > 0, frame['sigSum'] < mid_category), np.logical_and(frame['sigSum'] >= mid_category, frame['sigSum'] < high_category), frame['sigSum'] >= high_category]
        choices = ['zero', 'low', 'mid', 'high']
        frame['sigSumCat'] = np.select(cond, choices, default = choices[0])
        _df = frame['sigSumCat'].value_counts(dropna = False)

        df_category_counts[contig] = _df

    df_category_counts = df_category_counts.replace(np.nan, 0)
    df_category_counts[df_category_counts.columns] = df_category_counts[df_category_counts.columns].astype(int)
    
    adj_pvalue = pvalue_cutoff / (df_category_counts.shape[0] * df_category_counts.shape[1])
    threshold = abs(scipy.stats.norm.ppf(adj_pvalue / 2))
    
    table = sm.stats.Table(df_category_counts)

    # TODO:
    # save the table.standardized_resids to a file to be used in the report 

    df_sig_contigs = table.standardized_resids.T[table.standardized_resids.T['high'] > threshold]
    sig_contigs = df_sig_contigs.index.values

    df_contigs = df[['contig', 'contig_len']].drop_duplicates()
    circle = Gcircle()

    df_vectors = df_contigs[df_contigs['contig'].isin(vector_list)]
    df_vectors.sort_values('contig', axis = 0, ascending = True, inplace = True)

    df_sig_contigs = df_contigs[df_contigs['contig'].isin(sig_contigs)]
    df_sig_contigs = df_sig_contigs[~df_sig_contigs['contig'].isin(vector_list)]

    # remove the vectors and the contigs with a signal
    df_contigs = df_contigs[~df_contigs['contig'].isin(vector_list)]
    df_contigs = df_contigs[~df_contigs['contig'].isin(sig_contigs)]

    df_contigs.sort_values('contig_len', axis = 0, ascending = False, inplace = True)
    df_contigs = df_contigs[:(plot_segments - len(df_vectors) - len(df_sig_contigs))]

    df_contigs = pd.concat([df_contigs, df_sig_contigs])
    df_contigs.sort_values('contig', axis = 0, ascending = True, inplace = True, key = natsort_keygen())

    for (index, row) in df_vectors.iterrows():
        face_color = '#303030'
        edge_color = '#303030'
        
        contig_len = row['contig_len'] * VECTOR_SIZE_FACTOR

        if row['contig'] in sig_contigs:
            face_color = 'orangered'
            edge_color = 'orangered'

        spacing = 0
        if index == len(df_vectors) - 1:
            spacing = 2

        arc = Garc(arc_id = row['contig'], facecolor = face_color, edgecolor = edge_color, interspace = spacing, size = contig_len, raxis_range = (990, 1000), labelposition = 60, label_visible = True)
        circle.add_garc(arc)

    for (_, row) in df_contigs.iterrows():
        face_color = '#303030'
        edge_color = '#303030'
        
        contig_len = row['contig_len']

        if row['contig'] in sig_contigs:
            face_color = 'orangered'
            edge_color = 'orangered'

        arc = Garc(arc_id = row['contig'], facecolor = face_color, edgecolor = edge_color, interspace = 0, size = row['contig_len'], raxis_range = (990, 1000), labelposition = 60, label_visible = True)
        circle.add_garc(arc)

    _df = DataFrame()
    circle.set_garcs()

    # take care of log10(0) which can fail and log10(x): x < 1 which results in negative values
    if top_percentile < 1:
        top_percentile = 1 

    max_coverage = max(df['coverage'])
    if max_coverage < 1:
        max_coverage = 1

    (vmin, vmax) = (math.log10(top_percentile), math.log10(max_coverage))
    for (contig, frame) in df.groupby('contig'):
        if not contig in list(df_contigs['contig']) and not contig in list(df_vectors['contig']):
            continue

        frame['sigSum'] = frame['perc1'].rolling(rolling_segments, center = True).sum()
        frame['sigSum'] = frame['sigSum'].fillna(0)
        frame['sigSum'] = frame['sigSum'].astype(int, errors = 'ignore')

        cond = [frame['sigSum'] == 0, np.logical_and(frame['sigSum'] > 0, frame['sigSum'] < mid_category), np.logical_and(frame['sigSum'] >= mid_category, frame['sigSum'] < high_category), frame['sigSum'] >= high_category]
        choices = ['zero', 'low', 'mid', 'high']
        frame['sigSumCat'] = np.select(cond, choices, default = choices[0])

        _df = pd.concat([_df, frame])


        # keep only the low and mid categories
        # filter out the zero category
        df_contig = frame[frame['sigSumCat'] != 'zero']
        # save the high category
        df_high = df_contig[df_contig['sigSumCat'] == 'high']
        # filter out the high category
        df_contig = df_contig[df_contig['sigSumCat'] != 'high']
        
        if not contig in vector_list:
            # filter out 7/8ths of the 'low' and 'mid' segments
            df_contig = df_contig.iloc[::8]
            # filter out 2/3rds of the 'high' segments
            df_high = df_high.iloc[::3]
            circle.scatterplot(contig, data = np.log10(df_contig['coverage']), positions = df_contig['start'], rlim = [vmin-0.10*vmin, vmax+0.05*vmax], raxis_range=(600, 990), facecolor = 'black', spine = True)
            circle.scatterplot(contig, data = np.log10(df_high['coverage']), positions = df_high['start'], rlim = [vmin-0.10*vmin, vmax+0.05*vmax], raxis_range=(600, 990), facecolor = 'orangered', spine = True)
        else:
            circle.scatterplot(contig, data = np.log10(df_contig['coverage']), positions = df_contig['start'] * VECTOR_SIZE_FACTOR, rlim = [vmin-0.10*vmin, vmax+0.05*vmax], raxis_range=(600, 990), facecolor = 'black', spine = True)
            circle.scatterplot(contig, data = np.log10(df_high['coverage']), positions = df_high['start'] * VECTOR_SIZE_FACTOR, rlim = [vmin-0.10*vmin, vmax+0.05*vmax], raxis_range=(600, 990), facecolor = 'orangered', spine = True)

    # rename the columns for the following script
    _df.columns = ['Chromosome', 'start', 'end', 'scafId', 'count', 'bpCovered', 'regionLen', 'fracCovered', 'scaffoldLength', 'perc1', 'sigSum', 'sigSumCategory']
    
    _df['perc1'].replace(0, False, inplace = True)
    _df['perc1'].replace(1, True, inplace = True)

    output_file_name = os.path.join(shared_folder, f'{trackname}{output_ext}')
    _df.to_csv(output_file_name, sep = '\t', index = False)

    # write the plot to a file
    circle.figure.set_alpha(0.0)
    plot_file_name = os.path.join(shared_folder, f'{trackname}{plot_ext}')
    if plot_ext.endswith('png'):
        circle.figure.savefig(plot_file_name, bbox_inches = 'tight', dpi = 200)
    elif plot_ext.endswith(('pdf', 'eps', 'ps', 'svg')):
        circle.figure.savefig(plot_file_name, bbox_inches = 'tight')
    else:
        plot_file_name = f'{plot_file_name}.pdf'
        circle.figure.savefig(plot_file_name, bbox_inches = 'tight')

if __name__ == '__main__':
    main()
