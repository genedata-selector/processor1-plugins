#!/bin/bash
set -e
cmd="conda activate scrnaseq_0.1.0"
source $GDPROOT/bin/admin/plugin_env

ssw_command_handler "$cmd"

RMDPath="${plugin_folder}/analyse.Rmd"
inputPrefix="${global_shared_folder}/monocle/cds"
outputPrefix="${global_shared_folder}/monocle/cds"
ofile="${global_shared_folder}/monocle/DifferentialExpression.html"
dataFile=`pwd`"/0_output_data.txt"
rowFile=`pwd`"/0_output_rows.txt"

mkdir -p "${global_shared_folder}/monocle"  # R won't make the directory
params="inputPrefix='${inputPrefix}', outputPrefix='${outputPrefix}'"
params="${params}, PCA_nUMI='${PCA_nUMI}', PCA_num_genes_expressed='${PCA_num_genes_expressed}', PCA_cell_cycle='${PCA_cell_cycle}'"
params="${params}, ClusterLabel='${ClusterLabel}', txtFile='${shared_folder}/DEGenes.txt', threads='${max_threads}'"
params="${params}, dataFile='${dataFile}', rowFile='${rowFile}', nSamples='${total_samples_0}'"
CMD="rmarkdown::render('$RMDPath', output_file='${ofile}', params=list(${params}))"
echo ${CMD}
Rscript -e "$CMD"
cp ${shared_folder}/DEGenes.txt "${global_shared_folder}/monocle/DifferentialExpression.txt"
#cp ${plugin_folder}/*.txt ./
#CMD="rmarkdown::render('$RMDPath', output_file='${pdfPath}', output_format='pdf_document', params=list(inputPath='${inputPath}', mitoFile='${mitoFile}', rrnaFile='${rrnaFile}'))"
#Rscript -e "$CMD"
rm -f *.log  # rmarkdown::render produces a tex log
rm 0_output_rows.txt

if [[ $exportData == "true" ]] ; then
    mkdir -p "${registration_folder}"
    cp "${ofile}" "${registration_folder}"
fi
