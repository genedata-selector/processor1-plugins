---
title: "Monocle - Load and Filter"
output:
  html_document:
    toc: true
    toc_float: true
params:
  inputPath: "Salmon"
  outputPrefix: "Filtering_0000.RData"
  filterList: "filterList.txt"
  mitoFile: "mitolist.txt"    # Should be in the input genome directory!
  rrnaFile: "rRNAlist.txt"    # Should be in the input genome directory!
  ## Filtering cells
  # MT - should check for presence of mitoFile!
  filterMt: TRUE
  filterMtMin: 0
  filterMtMax: 0.15
  # rRNA - should check for presence of rrnaFile
  filterRrna: FALSE
  filterRrnaMin: 0
  filterRrnaMax: 1.0
  # nUMIs
  filterNUmis: TRUE
  filterNUmisType: "multiple"  # MAD, multiple, threshold
  filterNUmisConstant: 2.5  # for MAD and multiple
  filterNUmisMin: 0  # for threshold
  filterNUmisMax: 1e6  # for threshold
  # nGenes
  filterNGenes: FALSE
  filterNGenesType: "multiple"  # MAD, multiple, threshold
  filterNGenesConstant: 2.5  # for MAD and multiple
  filterNGenesMin: 0  # for threshold
  filterNGenesMax: 1e6  # for threshold
  ## Filtering Genes
  filterExpressedCells: TRUE
  filterExpressedCellsMin: 100  # no max
  filterMinUmi: TRUE
  filterMinUmiValue: 4
  filterMaxUmi: FALSE
  filterMaxUmiValue: 1e7
  filterMtGenes: TRUE
  filterRrnaGenes: TRUE
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

## Loading Data

```{r step1, include=FALSE}
library(monocle)
library(tximeta)
library(fishpond)
library(SingleCellExperiment)
library(scran)
library(details)
#library(Seurat)  # Fails on windows

basedirs = list.dirs(params$inputPath, recursive=F)
quantfiles = list.files(params$inputPath, pattern="quants_mat.gz", recursive=T, full.names=T)
SampleNames = basename(dirname(dirname(quantfiles)))

# This requires setup!!!
ses = lapply(quantfiles, function(x) tximeta(x, type="alevin", dropInfReps=TRUE))
sces = lapply(ses, function(x) as(x, "SingleCellExperiment"))
allSces = do.call(cbind, sces)

colData(allSces)$Sample = rep(SampleNames, times=sapply(sces, ncol))
colnames(allSces) = paste(colData(allSces)$Sample, colnames(allSces), sep="_")
beforePlotList = list()
afterPlotList = list()
beforePlotListGenes = list()
afterPlotListGenes = list()
```

The following samples were found: `r SampleNames`.

```{r step2, include=FALSE, message=FALSE, warning=FALSE}
# plot QC metrics - one for each sample!

# Make the CellDataSet
cds = convertTo(allSces, type="monocle", lowerDetectionLimit=0.5, expressionFamily=negbinomial())
paste("Raw data for", dim(cds)[2], "and", dim(cds)[1], "genes captured")

# Begin labeling gene types
fData(cds)$Type = "gene"
fData(cds)$maxExp = apply(exprs(cds), 1, max)
fData(cds)$expCells = apply(exprs(cds), 1, function(x) sum(x>=0.1))

# Begin adding cell-level metrics
pData(cds)$nUMIs = Matrix::colSums(exprs(cds))
cds = detectGenes(cds, min_expr = 0.1)  # add num_genes_expressed

RMV = rep(FALSE, ncol(cds))
if(file.exists(params$mitoFile)) {
  mitos = read.delim(params$mitoFile, header=FALSE)$V1
  mitos = mitos[which(mitos %in% rownames(allSces))]
  fData(cds)$Type[which(rownames(cds) %in% mitos)] = "MT"
  pData(cds)$Total_MT = Matrix::colSums(exprs(cds[which(fData(cds)$Type == "MT")]))
  pData(cds)$PercentMT = 100 * pData(cds)$Total_MT/pData(cds)$nUMIs
  if(params$filterMt) {
    RMV = RMV | (pData(cds)$PercentMT < as.numeric(params$filterMtMin))
    RMV = RMV | (pData(cds)$PercentMT > as.numeric(params$filterMtMax))
  }
}

cat(sprintf("MT %i\n", sum(RMV)))
if(file.exists(params$rrnaFile)) {
  rrnas = read.delim(params$rrnaFile, header=FALSE)$V1
  rrnas = rrnas[which(rrnas %in% rownames(allSces))]
  fData(cds)$Type[which(rownames(cds) %in% rrnas)] = "rRNA"
  pData(cds)$Total_rRNA = Matrix::colSums(exprs(cds[which(fData(cds)$Type == "rRNA")]))
  pData(cds)$PercentRRna = 100 * pData(cds)$Total_rRNA/pData(cds)$nUMIs
  if(params$filterRrna) {
    RMV = RMV | (pData(cds)$PercentRRna < as.numeric(params$filterRrnaMin))
    RMV = RMV | (pData(cds)$PercentRRna > as.numeric(params$filterRrnaMax))
  }
}

cat(sprintf("rRNA %i\n", sum(RMV)))
if(params$filterNUmis) {
  #MAD, multiple, threshold
  if(params$filterNUmisType == "multiple") {
    upperThreshold = median(pData(cds)$nUMIs) * as.numeric(params$filterNUmisConstant)
    lowerThreshold = median(pData(cds)$nUMIs) / as.numeric(params$filterNUmisConstant)
  } else if(params$filterNUmisType == "threshold") {
    upperThreshold = as.numeric(params$filterNUmisMax)
    lowerThreshold = as.numeric(params$filterNUmisMin)
  } else if(params$filterNUmisType == "MAD") {
    v = mad(pData(cds)$nUMIs)
    m = median(pData(cds)$nUMIs)
    upperThreshold = m + as.numeric(params$filterNUmisConstant) * v
    lowerThreshold = m - as.numeric(params$filterNUmisConstant) * v
  }
  RMV = RMV | (pData(cds)$nUMIs > upperThreshold)
  RMV = RMV | (pData(cds)$nUMIs < lowerThreshold)
}
cat(sprintf("nUMIs %i\n", sum(RMV)))
if(params$filterNGenes) {
  #MAD, multiple, threshold
  if(params$filterNGenesType == "multiple") {
    upperThreshold = median(pData(cds)$num_genes_expressed) * as.numeric(params$filterNGenesConstant)
    lowerThreshold = median(pData(cds)$num_genes_expressed) / as.numeric(params$filterNGenesConstant)
  } else if(params$filterNGenesType == "threshold") {
    upperThreshold = as.numeric(params$filterNGenesMax)
    lowerThreshold = as.numeric(params$filterNGenesMin)
  } else if(params$filterNGenesType == "MAD") {
    v = mad(pData(cds)$num_genes_expressed)
    m = median(pData(cds)$num_genes_expressed)
    upperThreshold = m + as.numeric(params$filterNGenesConstant) * v
    lowerThreshold = m - as.numeric(params$filterNGenesConstant) * v
  }
  RMV = RMV | (pData(cds)$num_genes_expressed > upperThreshold)
  RMV = RMV | (pData(cds)$num_genes_expressed < lowerThreshold)
}
cat(sprintf("nGenes %i\n", sum(RMV)))

# Label cells that WILL be filtered
if(sum(RMV) > 0) {
  pData(cds)$Type[RMV] = "Filtered"
}

# Create plots here
if(file.exists(params$mitoFile)) {
    g = ggplot(pData(cds), aes(x=Sample, y=PercentMT)) + geom_violin(aes(fill=Sample))
    g = g + theme(legend.position="none") + labs(x="", y="% MT")
    beforePlotList[[length(beforePlotList) + 1]] = g
}
if(file.exists(params$rrnaFile)) {
    g = ggplot(pData(cds), aes(x=Sample, y=PercentRRna)) + geom_violin(aes(fill=Sample))
    g = g + theme(legend.position="none") + labs(x="", y="% rRNA")
    beforePlotList[[length(beforePlotList) + 1]] = g
}
g = ggplot(pData(cds), aes(x=Sample, y=nUMIs)) + geom_violin(aes(fill=Sample)) + labs(x="", y="# UMIs/cell")
g = g + theme(legend.position="none")
g = g + scale_y_continuous(trans='log10')
beforePlotList[[length(beforePlotList) + 1]] = g
g = ggplot(pData(cds), aes(x=Sample, y=num_genes_expressed)) + geom_violin(aes(fill=Sample)) + labs(x="", y="# Genes/cell")
g = g + theme(legend.position="none")
g = g + scale_y_continuous(trans='log10')
beforePlotList[[length(beforePlotList) + 1]] = g

# UMI/gene plots
g = ggplot(fData(cds), aes(x=expCells)) + geom_density(fill="#F8766D") + labs(x="# Expressing cells", y="")
g = g + theme(legend.position="none")
g = g + scale_x_continuous(trans='log10')
beforePlotListGenes[[length(beforePlotListGenes) + 1]] = g
g = ggplot(fData(cds), aes(x=maxExp)) + geom_density(fill="#F8766D") + labs(x="Maximum UMIs", y="")
g = g + theme(legend.position="none")
g = g + scale_x_continuous(trans='log10')
beforePlotListGenes[[length(beforePlotListGenes) + 1]] = g

# Filter!
if(sum(RMV) > 0) {
  cds = cds[,-which(RMV)]
}

# Gene filtering
RMVGENES = rep(FALSE, nrow(cds))
if(params$filterExpressedCells == "TRUE") {
  RMVGENES = RMVGENES | fData(cds)$expCells < as.numeric(params$filterExpressedCellsMin)
}
if(params$filterMinUmi == "TRUE") {
  RMVGENES = RMVGENES | fData(cds)$maxExp < as.numeric(params$filterMinUmiValue)
}
if(params$filterMaxUmi == "TRUE") {
  RMVGENES = RMVGENES | fData(cds)$maxExp > as.numeric(params$filterMaxUmiValue)
}
if(params$filterMtGenes == "TRUE") {
  RMVGENES = RMVGENES | fData(cds)$Type == "MT"
}
if(params$filterRrnaGenes == "TRUE") {
  RMVGENES = RMVGENES | fData(cds)$Type == "rRNA"
}
write.table(data.frame(Removed=RMVGENES, row.names=rownames(fData(cds))), file=params$filterList, sep="\t", col.names=TRUE, row.names=TRUE, quote=FALSE)
if(sum(RMVGENES) > 0) {
    cds = cds[-which(RMVGENES)]
}

# After plots here
if(file.exists(params$mitoFile)) {
    g = ggplot(pData(cds), aes(x=Sample, y=PercentMT)) + geom_violin(aes(fill=Sample)) + labs(x="", y="%MT")
    g = g + theme(legend.position="none")
    afterPlotList[[length(afterPlotList) + 1]] = g
}
if(file.exists(params$rrnaFile)) {
    g = ggplot(pData(cds), aes(x=Sample, y=PercentRRna)) + geom_violin(aes(fill=Sample)) + labs(x="", y="%rRNA")
    g = g + theme(legend.position="none")
    afterPlotList[[length(afterPlotList) + 1]] = g
}
g = ggplot(pData(cds), aes(x=Sample, y=nUMIs)) + geom_violin(aes(fill=Sample)) + labs(x="", y="# UMIs/cell")
g = g + theme(legend.position="none")
g = g + scale_y_continuous(trans='log10')
afterPlotList[[length(afterPlotList) + 1]] = g
g = ggplot(pData(cds), aes(x=Sample, y=num_genes_expressed)) + geom_violin(aes(fill=Sample)) + labs(x="", y="# Genes/cell")
g = g + theme(legend.position="none")
g = g + scale_y_continuous(trans='log10')
afterPlotList[[length(afterPlotList) + 1]] = g

# UMI/gene plots
g = ggplot(fData(cds), aes(x=expCells)) + geom_density(fill="#F8766D") + labs(x="# Expressing cells", y="")
g = g + theme(legend.position="none")
g = g + scale_x_continuous(trans='log10')
afterPlotListGenes[[length(afterPlotListGenes) + 1]] = g
g = ggplot(fData(cds), aes(x=maxExp)) + geom_density(fill="#F8766D") + labs(x="Maximum UMIs", y="")
g = g + theme(legend.position="none")
g = g + scale_x_continuous(trans='log10')
afterPlotListGenes[[length(afterPlotListGenes) + 1]] = g
```

# Cell filtering {.tabset .tabset-fade .tabset-pills}
## Before filtering
```{r, echo=FALSE, fig.height=8}
gridExtra::grid.arrange(grobs=beforePlotList, ncol=1)
```

## After filtering
```{r, echo=FALSE, fig.height=8}
gridExtra::grid.arrange(grobs=afterPlotList, ncol=1)
```

# Gene filtering {.tabset .tabset-fade .tabset-pills}
## Before filtering
```{r, echo=FALSE, warnings=FALSE, fig.height=8}
gridExtra::grid.arrange(grobs=beforePlotListGenes, ncol=1)
```

## After filtering
```{r, echo=FALSE, warnings=FALSE, fig.height=8}
gridExtra::grid.arrange(grobs=afterPlotListGenes, ncol=1)
```

# Summary

Using the specified filtering parameters removes `r sum(RMV)` of `r length(RMV)` cells and `r sum(RMVGENES)` of `r length(RMVGENES)` genes.

# Parameters {.tabset .tabset-fade .tabset-pills}
## Cell filtering
::::{style="display: flex;"}
::: {}
```{r, echo=FALSE}
df_params = t(as.data.frame(params))
colnames(df_params) = "Values"

cellTable = data.frame("Setting"=c("Filter by MT %?", "Minimum MT %", "Maximum MT %", "Filter by # UMIs?", "Type of UMI filter", "UMI filtering constant", "Min. UMIs", "Max. UMIs"),
                       "Value"=c(params$filterMt, params$filterMtMin, params$filterMtMax, params$filterNUmis, params$filterNUmisType, params$filterNUmisConstant, params$filterNUmisMin, params$filterNUmisMax))
knitr::kable(cellTable)
```
:::

::: {.col data-latex="{0.46\textwidth}"}
```{r, echo=FALSE}
cellTable = data.frame("Setting"=c("Filter by rRNA %?", "Minimum rRNA %", "Maximum rRNA %", "Filter by # genes?", "Type of gene filter", "Gene filtering constant", "Min. genes", "Max. genes"),
                       "Value"=c(params$filterRrna, params$filterRrnaMin, params$filterRrnaMax, params$filterNGenes, params$filterNGenesType, params$filterNGenesConstant, params$filterNGenesMin, params$filterNGenesMax))
knitr::kable(cellTable)
```
:::
::::

## Gene filtering

```{r, echo=FALSE}
cellTable = data.frame("Setting"=c("Filter by # cells expressing?", "Min. Expressed Cells", "Filter by min. UMI count?", "Min. UMI count", "Filter by max. UMI count?", "Max. UMI count", "Exclude MT genes?", "Exclude rRNA genes?"),
                       Values=c(params$filterExpressedCells, params$filterExpressedCellsMin, params$filterMinUmi, params$filterMinUmiValue, params$filterMaxUmi, params$filterMaxUmiValue, params$filterMtGenes, params$filterRrnaGenes))
knitr::kable(cellTable)
```

# Session information

```{r, echo=FALSE}
getOutputFile <- function(prefix) {
    for(offset in c(0:9999)) {
        oname = sprintf("%s_%04i.RData", prefix, offset)
        if(!file.exists(oname)) return(oname)
    }
}
save(cds, RMVGENES, file=getOutputFile(params$outputPrefix))
```
```{details, echo = FALSE, details.summary = 'current session info'}
sessionInfo()
```
