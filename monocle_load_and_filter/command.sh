#!/bin/bash
set -e
cmd="conda activate scrnaseq_0.1.0"
source $GDPROOT/bin/admin/plugin_env

ssw_command_handler "$cmd"

RMDPath="${plugin_folder}/analyse.Rmd"
inputPath="${global_shared_folder}/salmon"
ofile="${global_shared_folder}/monocle/filtering.html"
filterList="${global_shared_folder}/monocle/filterList.txt"
outputPrefix="${global_shared_folder}/monocle/cds"

mkdir -p "${global_shared_folder}/monocle"  # R won't make the directory
params="inputPath='${inputPath}', outputPrefix='${outputPrefix}', mitoFile='${mitoFile}', rrnaFile='${rrnaFile}'"
# Cell filtering
params="${params}, filterMt='${filterMt}', filterMtMin='${filterMtMin}', filterMtMax='${filterMtMax}'"
params="${params}, filterRrna='${filterRrna}', filterRrnaMin='${filterRrnaMin}', filterRrnaMax='${filterRrnaMax}'"
params="${params}, filterNUmis='${filterNUmis}', filterNUmisType='${filterNUmisType}', filterNUmisConstant='${filterNUmisConstant}', filterNUmisMin='${filterNUmisMin}', filterNUmisMax='${filterNUmisMax}'"
params="${params}, filterNGenes='${filterNGenes}', filterNGenesType='${filterNGenesType}', filterNGenesConstant='${filterNGenesConstant}', filterNGenesMin='${filterNGenesMin}', filterNGenesMax='${filterNGenesMax}'"
# Gene filtering
params="${params}, filterExpressedCells='${filterExpressedCells}', filterExpressedCellsMin='${filterExpressedCellsMin}'"
params="${params}, filterMinUmi='${filterMinUmi}',   filterMinUmiValue='${filterMinUmiValue}', filterMaxUmi='${filterMaxUmi}', filterMaxUmiValue='${filterMaxUmiValue}'"
params="${params}, filterMtGenes='${filterMtGenes}', filterRrnaGenes='${filterRrnaGenes}', filterList='${filterList}'"

CMD="rmarkdown::render('$RMDPath', output_file='${ofile}', params=list(${params}))"
Rscript -e "$CMD"
#CMD="rmarkdown::render('$RMDPath', output_file='${pdfPath}', output_format='pdf_document', params=list(inputPath='${inputPath}', mitoFile='${mitoFile}', rrnaFile='${rrnaFile}'))"
#Rscript -e "$CMD"
rm -f *.log  # rmarkdown::render produces a text log in the cwd

if [[ $exportData == "true" ]] ; then
    cp "${ofile}" "${registration_folder}"
fi
