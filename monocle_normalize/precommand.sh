#!/bin/bash
set -e

export activity_name="scrnaseq_0.1.0"
echo "Using ${activity_name}"

# Sourcing script containing basic functions for conda
source $GDPROOT/bin/admin/plugin_env

# Set the file with the conda environment
tar_ball="${plugin_folder}/../monocle_load_and_filter/${activity_name}.tar.gz"

# Install the environment if not already installed
install_env "${activity_name}" "${tar_ball}"
