#!/bin/bash
set -e
cmd="conda activate scrnaseq_0.1.0"
source $GDPROOT/bin/admin/plugin_env

ssw_command_handler "$cmd"

RMDPath="${plugin_folder}/analyse.Rmd"
inputPrefix="${global_shared_folder}/monocle/cds"
outputPrefix="${global_shared_folder}/monocle/cds"
ofile="${global_shared_folder}/monocle/normalize.html"

params="inputPrefix='${inputPrefix}', outputPrefix='${outputPrefix}', minExpThres='${minExpThres}'"

CMD="rmarkdown::render('$RMDPath', output_file='${ofile}', params=list(${params}))"
echo "$CMD"
Rscript -e "$CMD"
#CMD="rmarkdown::render('$RMDPath', output_file='${pdfPath}', output_format='pdf_document', params=list(inputPath='${inputPath}', mitoFile='${mitoFile}', rrnaFile='${rrnaFile}'))"
#Rscript -e "$CMD"
rm -f *.log

if [[ $exportData == "true" ]] ; then
    cp "${ofile}" "${registration_folder}"
fi
